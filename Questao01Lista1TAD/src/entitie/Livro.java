package entitie;

import service.LivroSet;

import java.util.List;

public class Livro implements LivroSet {
	
	String nome;
	String editora;
	int anoPublicacao;
	
	public Livro() {
		
	}
	
	public Livro(String nome, String editora, int anoPublicacao) {
		this.nome = nome;
		this.editora = editora;
		this.anoPublicacao = anoPublicacao;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getEditora() {
		return editora;
	}
	
	public void setEditora(String editora) {
		this.editora = editora;
	}
	
	public int getAno() {
		return anoPublicacao;
	}
	
	public void setAno(int anoPublicacao) {
		this.anoPublicacao = anoPublicacao;
	}
	
	@Override
	public boolean contains(List<Livro> livro, Livro livro1) {
		boolean consultar;
		consultar = livro.contains(livro1);
		return consultar;
	}

	@Override
	public boolean add(List<Livro> livro, Livro livro1) {
		boolean adicionar;
		adicionar = livro.add(livro1);
		return adicionar;
	}

	@Override
	public boolean remove(List<Livro> livro, Livro livro1) {
		boolean remover;
		remover = livro.remove(livro1);
		return remover;
	}

	@Override
	public int size(List<Livro> livro) {
		return livro.size();
	}

	@Override
	public void clear(List<Livro> livro) {
		livro.clear();
	}
		
}