package main;

import entitie.Livro;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

	static List<Livro> livro = new ArrayList<>();
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		String nome, editora, r;
		int anoPublicacao;
		Livro l = new Livro();

		char escolha = 's';
		while (escolha == 's') {
			System.out.println("Informe o nome do livro que deseja salvar: ");
			nome = sc.nextLine();
			System.out.println("Informe a editora: ");
			editora = sc.nextLine();
			System.out.println("Informe o ano de publica��o: ");
			anoPublicacao = sc.nextInt();
			sc.nextLine();
			
			l = new Livro();
			l.add(livro, l);
			System.out.println("Deseja armazenar outro livro? (s) sim      (n) n�o");
			escolha = sc.nextLine().charAt(0);
		}

		System.out.println("Deseja consultar os livros? (s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			String teste = " ";
			if (l.contains(livro, l)) {
				teste = "existem.";
			} else {
				teste = "n�o existem.";
			}
			System.out.println("Os livros " + teste);
		}

		System.out.println("Deseja remover um livro? (s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			l.remove(livro, l);
			System.out.println("Livro removido!");
		}

		System.out.println("A quantidade de livros � " + l.size(livro));

		System.out.println("Deseja limpar a lista de livros? (s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			l.clear(livro);
			System.out.println("Lista limpa!");
			System.out.println("A quantidade de livros agora � " + l.size(livro));
		} else {
			System.out.println("A quantidade de livros � " + l.size(livro));
		}
	}

}
