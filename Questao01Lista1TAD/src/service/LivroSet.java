package service;

import entitie.Livro;

import java.util.List;

public interface LivroSet {
	
	public boolean contains(List<Livro> livro, Livro livro1);
	
	public boolean add(List<Livro> livro, Livro livro1);
	
	public boolean remove(List<Livro> livro, Livro livro1);
	
	public int size(List<Livro> livro);
	
	public void clear(List<Livro> livro);
	
}
