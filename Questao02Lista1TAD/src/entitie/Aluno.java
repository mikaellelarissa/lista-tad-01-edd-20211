package entitie;

import service.AlunoSet;

import java.util.List;

public class Aluno implements AlunoSet {
	
	String nome;
	int semestre;
	
	public Aluno() {
		
	}

	public Aluno(String nome, int semestre) {
		this.nome = nome;
		this.semestre = semestre;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getSemestre() {
		return semestre;
	}
	
	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}

	@Override
	public boolean contains(List<Aluno> aluno, Aluno aluno1) {
		boolean consultar;
		consultar = aluno.contains(aluno1);
		return consultar;
	}

	@Override
	public boolean add(List<Aluno> aluno, Aluno aluno1) {
		boolean adicionar;
		adicionar = aluno.add(aluno1);
		return adicionar;
	}

	@Override
	public boolean remove(List<Aluno> aluno, Aluno aluno1) {
		boolean remover;
		remover = aluno.remove(aluno1);
		return remover;
	}

	@Override
	public int size(List<Aluno> aluno) {
		return aluno.size();
	}

	@Override
	public void clear(List<Aluno> aluno) {
		aluno.clear();
	}
}
