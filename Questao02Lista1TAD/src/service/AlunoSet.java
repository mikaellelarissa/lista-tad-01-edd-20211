package service;

import entitie.Aluno;

import java.util.List;

public interface AlunoSet {

	public boolean contains(List<Aluno> aluno, Aluno aluno1);

	public boolean add(List<Aluno> aluno, Aluno aluno1);

	public boolean remove(List<Aluno> aluno, Aluno aluno1);

	public int size(List<Aluno> aluno);

	public void clear(List<Aluno> aluno);

}
