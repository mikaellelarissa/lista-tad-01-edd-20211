package main;

import entitie.Aluno;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {
	
	static List<Aluno> aluno = new ArrayList<>();
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		String nome, r;
		int semestre;
		Aluno a = new Aluno();
		
		char escolha = 's';
		while(escolha == 's') {
			System.out.println("Informe o nome do aluno: ");
			nome = sc.nextLine();
			System.out.println("Informe o semestre do aluno: ");
			semestre = sc.nextInt();
			sc.nextLine();
			
			a = new Aluno();
			a.add(aluno, a);
			System.out.println("Deseja cadastrar um novo aluno? (s) sim      (n) n�o");
			escolha = sc.nextLine().charAt(0);
		}
		
		System.out.println("Deseja consultar um aluno? (s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			String teste = " ";
			if (a.contains(aluno, a)) {
				teste = "existe.";
			} else {
				teste = "n�o existe.";
			}
			System.out.println("O aluno " + teste);
		}
		
		System.out.println("Deseja remover um aluno? (s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			a.remove(aluno, a);
			System.out.println("Aluno removido!");
		}
		
		System.out.println("A quantidade de alunos � " + a.size(aluno));

		System.out.println("Deseja limpar a lista de alunos? (s) sim      (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			a.clear(aluno);
			System.out.println("Lista limpa!");
			System.out.println("A quantidade de alunos agora � " + a.size(aluno));
		} else {
			System.out.println("A quantidade de alunos � " + a.size(aluno));
		}
	}

}
