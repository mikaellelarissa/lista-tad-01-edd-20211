package service;

public interface NumeroSet {

	public String create(int n1, int n2);
	
	public double add(int n1, int n2);
	
	public double multiply(int n1, int n2);
	
	public String test(int n1, int n2);
	
}
