package entitie;

import service.NumeroSet;

public class Numero implements NumeroSet {

	public Numero() {

	}

	@Override
	public String create(int n1, int n2) {
		return n1 +"/" + n2;
	}

	@Override
	public double add(int n1, int n2) {
		double soma;
		soma = n1 + n2;
		return soma;
	}

	@Override
	public double multiply(int n1, int n2) {
		double mult;
		mult = n1 * n2;
		return mult;
	}

	@Override
	public String test(int n1, int n2) {
		String teste;
		if(n1 == n2) {
			teste = " s�o iguais.";
		} else {
			teste = " s�o diferentes.";
		}
		return teste;
	}

}