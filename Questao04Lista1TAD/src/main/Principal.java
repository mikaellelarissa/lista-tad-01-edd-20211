package main;

import entitie.Numero;

import java.util.Scanner;

public class Principal {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int n1, n2;
		int operacao = 0;
		Numero n = new Numero();

			System.out.println("Informe um valor: ");
			n1 = sc.nextInt();
			System.out.println("Informe um valor: ");
			n2 = sc.nextInt();
			sc.nextLine();

			n.create(n1, n2);

		char r = 's';
		System.out.println("Deseja realizar alguma opera��o? (s) sim   (n) n�o");
		r = sc.nextLine().charAt(0);
		while (r == 's') {
			System.out.println(
					"(1) Somar racionais" + "\n(2) Multiplicar racionais" + "\n(3) Testar igualdade" + "\n(4) Fechar");
			operacao = sc.nextInt();
			if (operacao == 1) {
				System.out.println(n1 + " + " + n2 + " = " + n.add(n1, n2));
			} else if (operacao == 2) {
				System.out.println(n1 + " x " + n2 + " = " + n.multiply(n1, n2));
			} else if (operacao == 3) {
				System.out.println(n1 + " e " + n2 + n.test(n1, n2));
			} else if (operacao == 4) {
				System.out.println("Programa encerrado!");
				break;
			}
		}

	}
}