package service;

public interface ContaSet {
	
	public double view();
	
	public double deposit(double valor);
	
	public double withdraw(double valor);
	
	public void update(String nome, int numeroConta);
	
}
