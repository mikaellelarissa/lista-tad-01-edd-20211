package entitie;

import service.ContaSet;

public class Conta implements ContaSet {

	private int numeroConta;
	private String nome;
	private double saldo;
	
	public Conta() {
		
	}
	
	public Conta(int numeroConta, String nome, double saldo) {
		this.numeroConta = numeroConta;
		this.nome = nome;
		this.saldo = saldo;
	}
	
	public int getConta() {
		return numeroConta;
	}
	
	public void setConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	@Override
	public double view() {
		return saldo;
	}

	@Override
	public double deposit(double valor) {
		saldo = saldo + valor;
		return saldo;
	}

	@Override
	public double withdraw(double valor) {
		saldo = saldo - valor;
		return saldo;
	}

	@Override
	public void update(String nome, int numeroConta) {
		this.nome = nome;
		this.numeroConta = numeroConta;
	}
}
