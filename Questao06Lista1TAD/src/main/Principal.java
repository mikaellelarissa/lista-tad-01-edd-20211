package main;

import entitie.Agenda;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

	static Scanner sc = new Scanner(System.in);
	static List<Agenda> agenda = new ArrayList<>();

	public static void main(String[] args) {
		String nome, numero, r = " ";
		Agenda a = new Agenda();

		System.out.println("Deseja adicionar um contato? (s) sim    (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			char escolha = 's';
			while (escolha == 's') {
				System.out.println("Informe o nome do contato: ");
				nome = sc.nextLine();
				System.out.println("Informe o n�mero do contato: ");
				numero = sc.nextLine();
				
				a.add(agenda, a);
				System.out.println("Deseja adicionar outro contato? (s) sim    (n) n�o");
				escolha = sc.nextLine().charAt(0);
			}
		}
		
		System.out.println("Deseja excluir a agenda? (s) sim    (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			a.delete(agenda);
			System.out.println("Agenda vazia!");
		}		
		
		System.out.println("Deseja atualizar agenda? (s) sim    (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			System.out.println("Agenda atualizada!");
			a.update(agenda, a);
		}
		
		System.out.println("Deseja remover um contato? (s) sim    (n) n�o");
		r = sc.nextLine();
		if (r.equals("s")) {
			a.remove(agenda, a);
			System.out.println("Contato removido!");
		}
	}
}