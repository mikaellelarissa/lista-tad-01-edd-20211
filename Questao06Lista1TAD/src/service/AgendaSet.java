package service;

import entitie.Agenda;

import java.util.List;

public interface AgendaSet {
//Sabendo que as opera��es de uma agenda telef�nica possuem as
	//opera��es: Adicionar, excluir, atualizar, remover. Elabore uma TAD para
	//representar essas opera��es.
	
	public boolean add(List<Agenda> agenda, Agenda agenda1);
	
	public void delete(List <Agenda> agenda);
	
	public boolean update(List<Agenda> agenda, Agenda agenda1);
	
	public boolean remove(List<Agenda> agenda, Agenda agenda1);
	
}
