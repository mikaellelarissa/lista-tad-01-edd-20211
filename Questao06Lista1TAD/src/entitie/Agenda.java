package entitie;

import service.AgendaSet;

import java.util.List;

public class Agenda implements AgendaSet {

	String nome, numero;
	
	public Agenda() {
		
	}

	public Agenda(String nome, String numero) {
		this.nome = nome;
		this.numero = numero;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public boolean add(List<Agenda> agenda, Agenda agenda1) {
		boolean adicionar;
		adicionar = agenda.add(agenda1);
		return adicionar;
	}

	@Override
	public void delete(List<Agenda> agenda) {
		agenda.clear();
	}

	@Override
	public boolean update(List<Agenda> agenda, Agenda agenda1) {
		return false;
	}

	@Override
	public boolean remove(List<Agenda> agenda, Agenda agenda1) {
		boolean remover;
		remover = agenda.remove(agenda1);
		return remover;
	}
}
